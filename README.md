# Drupal 7 extension helper

Implements some Keycloak SPI extensions.

- `d7-username`: _SPI validator_ implementing Drupal 7 `user_validate_name()`
  function for Keycloak user profile attributes.
- `drupalorg-relay`: _SPI eventsListener_ implementing a relay of Keycloak
  events to a callback implemented on `drupalorg` project.

## Build

A jar to add to Keycloak can be generated with maven.
E.g.

    lando mvn clean install -Djakarta

## Use

### d7-username

- Go to _Realm settings > User profile > username > Validations_.
- Click _Add validator_.
- Choose _d7-username_ validator type, and _Save_.

### drupalorg-relay

- Go to _Realm settings > Events_.
- Add _drupalorg-relay_ event listener.

Make sure to add the following environment variables.

- `DRUPALORG_RELAY_ENDPOINT`: URL where a POST request is sent with metadata for
  post-processing a KC event. E.g. https://example.org/drupalorg-keycloak-event
- `DRUPALORG_RELAY_AUTH_TOKEN`: Token for authentication on the POST request,
  via `x-keycloak-token` header.

Optionally, set the following environment variables.

- `DRUPALORG_RELAY_BASIC_AUTH`: Basic authorization string, e.g.
  `drupal:drupal`.

## Run tests

    lando mvn test
