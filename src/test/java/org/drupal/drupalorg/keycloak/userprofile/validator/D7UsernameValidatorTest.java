package org.drupal.drupalorg.keycloak.userprofile.validator;

import java.io.UnsupportedEncodingException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;

class D7UsernameValidatorTest {

    @Test
    @DisplayName("Tests D7UsernameValidator.matchesRegex1()")
    void testMatchesRegex1() {
        D7UsernameValidator validator = D7UsernameValidator.INSTANCE;
        try {
            assertFalse(validator.matchesRegex1("foo"));
            assertFalse(validator.matchesRegex1("FOO"));
            assertFalse(validator.matchesRegex1("f'oo- b.ar+001@baz_ñoq"));
            assertFalse(validator.matchesRegex1("þÞòøÇßª"));
            assertFalse(validator.matchesRegex1("ř€"));
            assertFalse(validator.matchesRegex1("ᛇᚻ᛫ᛒᛦᚦ"));
            assertFalse(validator.matchesRegex1("combined þÞòøÇßªř€ī sets"));
        }
        catch (UnsupportedEncodingException e) {
            fail("Unexpected unsupported exception thrown");
        }
    }

    @Test
    @DisplayName("D7 UserValidationTestCase::testUsernames()")
    void testUsernameValidation() {
        D7UsernameValidator validator = D7UsernameValidator.INSTANCE;
        try {
            assertEquals(validator.OK, validator.validateUsername("foo"));
            assertEquals(validator.OK, validator.validateUsername("FOO"));
            assertEquals(validator.OK, validator.validateUsername("Foo O'Bar"));
            assertEquals(validator.OK, validator.validateUsername("foo@bar"));
            assertEquals(validator.OK, validator.validateUsername("foo@example.com"));
            assertEquals(validator.OK, validator.validateUsername("foo@-example.com"));
            assertEquals(validator.OK, validator.validateUsername("foo+bar"));
            assertEquals(validator.OK, validator.validateUsername("þòøÇßªř€"));
            assertEquals(validator.OK, validator.validateUsername("ᛇᚻ᛫ᛒᛦᚦ"));
            assertEquals(validator.MESSAGE_STARTS_WITH_SPACE, validator.validateUsername(" foo"));
            assertEquals(validator.MESSAGE_ENDS_WITH_SPACE, validator.validateUsername("foo "));
            assertEquals(validator.MESSAGE_TWO_SPACES, validator.validateUsername("foo  bar"));
            assertEquals(validator.MESSAGE_NOT_SET, validator.validateUsername(""));
            assertEquals(validator.MESSAGE_ILLEGAL_CHAR_1, validator.validateUsername("foo/"));
            char at_null = (char) 0;
            char at_cr = (char) 13;
            assertEquals(validator.MESSAGE_ILLEGAL_CHAR_1, validator.validateUsername("foo" + at_null + "bar"));
            assertEquals(validator.MESSAGE_ILLEGAL_CHAR_1, validator.validateUsername("foo" + at_cr + "bar"));
            String long_name = "x".repeat(validator.USERNAME_MAX_LENGTH + 1);
            assertEquals(validator.MESSAGE_TOO_LONG, validator.validateUsername(long_name));
        }
        catch (UnsupportedEncodingException e) {
            fail("Unexpected unsupported exception thrown");
        }
    }
}
