package org.drupal.drupalorg.keycloak.events.relay;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.Base64;
import java.util.Set;
import org.jboss.logging.Logger;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerTransaction;
import org.keycloak.events.EventType;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.models.KeycloakSessionTask;
import org.keycloak.models.RealmProvider;
import static org.keycloak.models.utils.KeycloakModelUtils.runJobInTransaction;

public class RelayEventListenerProvider implements EventListenerProvider {

    private static final Logger log = Logger.getLogger(RelayEventListenerProvider.class);
    private KeycloakSession session;
    private RealmProvider model;
    private Set<EventType> includedEvents;
    private EventListenerTransaction tx = new EventListenerTransaction(this::relayAdminEvent, this::relayEvent);
    private final KeycloakSessionFactory sessionFactory;

    public RelayEventListenerProvider(KeycloakSession session, Set<EventType> includedEvents) {
        this.session = session;
        this.model = session.realms();
        this.includedEvents = includedEvents;
        this.session.getTransactionManager().enlistAfterCompletion(tx);
        this.sessionFactory = session.getKeycloakSessionFactory();
    }

    @Override
    public void onEvent(Event event) {
        if (includedEvents.contains(event.getType())) {
            if (event.getRealmId() != null) {
                // Only pass along events with non-empty realm identifier.
                tx.addEvent(event);
            }
        }
    }

    private void relayEvent(Event event) {
        runJobInTransaction(sessionFactory, new KeycloakSessionTask() {
            @Override
            public void run(KeycloakSession session) {
                String relay_url = "/user/" + event.getType();
                String payload = RelayEventListenerProvider.getJsonRepresentation(event);
                RelayEventListenerProvider.relay(relay_url, payload);
            }
        });
    }

    private void relayAdminEvent(AdminEvent adminEvent, boolean includeRepresentation) {
        runJobInTransaction(sessionFactory, new KeycloakSessionTask() {
            @Override
            public void run(KeycloakSession session) {
                String payload = RelayEventListenerProvider.getJsonRepresentation(adminEvent);
                String relay_url = "/admin/" + adminEvent.getResourceTypeAsString();
                RelayEventListenerProvider.relay(relay_url, payload);
            }
        });
    }

    @Override
    public void onEvent(AdminEvent event, boolean includeRepresentation) {
        if (!this.shouldRelay(event)) {
            // Nothing to do.
            return;
        }
        tx.addAdminEvent(event, includeRepresentation);
    }

    /**
     * Helper to create a JSON string from a user Event.
     */
    public static String getJsonRepresentation(Event event) {
        // TODO Maybe use a proper JSON library like org.json, since Java does
        // not have one included over stdlib.
        return String.format("{"
            + "\"event_trigger\": \"user\","
            + "\"event\": {"
                + "\"id\": \"%s\","
                + "\"time\": \"%s\","
                + "\"type\": \"%s\","
                + "\"realm_id\": \"%s\","
                + "\"client_id\": \"%s\","
                + "\"session_id\": \"%s\","
                + "\"user_id\": \"%s\","
                + "\"ip_address\": \"%s\","
                + "\"error\": \"%s\""
                // TODO representation property not relayed for now, since
                // it is not currently used on d.o relay.
                + "}"
            + "}",
            event.getId(),
            event.getTime(),
            event.getType(),
            event.getRealmId(),
            event.getClientId(),
            event.getSessionId() == null ? "" : event.getSessionId(),
            event.getUserId() == null ? "" : event.getUserId(),
            event.getIpAddress(),
            event.getError() == null ? "" : event.getError()
        );
    }

    /**
     * Helper to create a JSON string from an AdminEvent.
     */
    public static String getJsonRepresentation(AdminEvent event) {
        // TODO Maybe use a proper JSON library like org.json, since Java does
        // not have one included over stdlib.
        return String.format("{"
            + "\"event_trigger\": \"admin\","
            + "\"event\": {"
                + "\"id\": \"%s\","
                + "\"time\": \"%s\","
                + "\"operation_type\": \"%s\","
                + "\"resource_type\": \"%s\","
                + "\"resource_path\": \"%s\","
                + "\"realm_id\": \"%s\","
                + "\"client_id\": \"%s\","
                + "\"user_id\": \"%s\","
                + "\"ip_address\": \"%s\","
                + "\"error\": \"%s\""
                // TODO representation property not relayed for now, since
                // it is not currently used on d.o relay.
                + "}"
            + "}",
            event.getId(),
            event.getTime(),
            event.getOperationType(),
            event.getResourceTypeAsString(),
            event.getResourcePath(),
            event.getAuthDetails().getRealmId(),
            event.getAuthDetails().getClientId(),
            event.getAuthDetails().getUserId(),
            event.getAuthDetails().getIpAddress(),
            event.getError() == null ? "" : event.getError()
        );
    }

    @Override
    public void close() {
        // No action on close.
    }

    /**
     * Helper to decide if an admin event should be relied.
     */
    private boolean shouldRelay(AdminEvent event) {
        String[] wanted_resource_types = {"USER", "USER_SESSION", "REALM"};
        return Arrays.asList(wanted_resource_types)
            .contains(event.getResourceTypeAsString());
    }

    /**
     * Helper to send HTTP POST to drupal.org.
     */
    public static void relay(String url_piece, String payload) {
        String relay_url = System.getenv("DRUPALORG_RELAY_ENDPOINT");
        if (relay_url == null) {
            log.error("drupalorg-relay: Cannot relay without DRUPALORG_RELAY_ENDPOINT environment variable");
            return;
        }
        String relay_auth_token = System.getenv("DRUPALORG_RELAY_AUTH_TOKEN");
        if (relay_auth_token == null) {
            log.error("drupalorg-relay: Cannot relay without DRUPALORG_RELAY_AUTH_TOKEN environment variable");
            return;
        }
        relay_url += url_piece;
        Builder request_builder = HttpRequest.newBuilder()
            .uri(URI.create(relay_url))
            .header("Content-Type", "application/json")
            .header("x-keycloak-token", relay_auth_token)
            .POST(HttpRequest.BodyPublishers.ofString(payload));
        String relay_basic_auth = System.getenv("DRUPALORG_RELAY_BASIC_AUTH");
        if (relay_basic_auth != null && relay_basic_auth.length() > 0) {
            String basic_authorization = Base64.getEncoder().encodeToString((relay_basic_auth).getBytes());
            request_builder.header("Authorization", "Basic " + basic_authorization);
        }
        HttpRequest request = request_builder.build();
        HttpClient client = HttpClient.newHttpClient();
        try {
            // TODO Maybe convert to asynchronous call.
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        }
        catch (Exception e) {
            log.error("drupalorg-relay: Failed to relay event", e);
        }
    }

}
