package org.drupal.drupalorg.keycloak.events.relay;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.keycloak.Config;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerProviderFactory;
import org.keycloak.events.EventType;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.provider.ProviderConfigurationBuilder;

public class RelayEventListenerProviderFactory implements EventListenerProviderFactory {

    public static final String ID = "drupalorg-relay";
    private static final Set<EventType> SUPPORTED_EVENTS = new HashSet<>();
    static {
        // Allow-list a small subset of events planned to be relayed.
        // See org.keycloak.events.EventType for the full list.
        Collections.addAll(SUPPORTED_EVENTS,
            EventType.DELETE_ACCOUNT,
            EventType.LOGOUT,
            EventType.LOGOUT_ERROR,
            EventType.UPDATE_EMAIL,
            EventType.UPDATE_PROFILE
        );
    }
    private Set<EventType> includedEvents = new HashSet<>();

    @Override
    public EventListenerProvider create(KeycloakSession session) {
        return new RelayEventListenerProvider(session, includedEvents);
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void init(Config.Scope config) {
        String[] include = config.getArray("include-events");
        if (include != null) {
            for (String i : include) {
                includedEvents.add(EventType.valueOf(i.toUpperCase()));
            }
        } else {
            includedEvents.addAll(SUPPORTED_EVENTS);
        }
        String[] exclude = config.getArray("exclude-events");
        if (exclude != null) {
            for (String e : exclude) {
                includedEvents.remove(EventType.valueOf(e.toUpperCase()));
            }
        }
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
    }

    @Override
    public void close() {
    }

    @Override
    public List<ProviderConfigProperty> getConfigMetadata() {
        String[] supportedEvents = Arrays.stream(EventType.values())
            .map(EventType::name)
            .map(String::toLowerCase)
            .sorted(Comparator.naturalOrder())
            .toArray(String[]::new);
        return ProviderConfigurationBuilder.create()
            .property()
            .name("include-events")
            .type("string")
            .helpText("A comma-separated list of events that should be relayed.")
            .options(supportedEvents)
            .defaultValue("All events")
            .add()
            .property()
            .name("exclude-events")
            .type("string")
            .helpText("A comma-separated list of events that should not be relayed.")
            .options(supportedEvents)
            .add()
            .build();
    }

}
