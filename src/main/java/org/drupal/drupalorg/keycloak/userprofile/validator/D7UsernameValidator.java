package org.drupal.drupalorg.keycloak.userprofile.validator;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import org.keycloak.provider.ConfiguredProvider;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.validate.AbstractStringValidator;
import org.keycloak.validate.ValidationContext;
import org.keycloak.validate.ValidationError;
import org.keycloak.validate.ValidatorConfig;

/**
 * A validator emulating D7 user_validate_name().
 *
 * Last checked related logic on Drupal side at version 7.102.
 * Exactly, a bit after that, on upstream commit
 * c3db7a9466ab0dff32de723f4a1e41ee447ad767.
 */
public class D7UsernameValidator extends AbstractStringValidator implements ConfiguredProvider {
    public static final D7UsernameValidator INSTANCE = new D7UsernameValidator();
    public static final String ID = "d7-username";
    public static final String MESSAGE_ENDS_WITH_SPACE = "name-error-ends-with-space";
    public static final String MESSAGE_ILLEGAL_CHAR_1 = "name-error-illegal-char-1";
    public static final String MESSAGE_ILLEGAL_CHAR_2 = "name-error-illegal-char-2";
    public static final String MESSAGE_NOT_SET = "name-error-not-set";
    public static final String MESSAGE_STARTS_WITH_SPACE = "name-error-starts-with-space";
    public static final String MESSAGE_TOO_LONG = "name-error-too-long";
    public static final String MESSAGE_TWO_SPACES = "name-error-two-spaces";
    public static final String MESSAGE_INVALID_ENCODING = "name-invalid-encoding";
    public static final String OK = "ok";
    public static final int USERNAME_MAX_LENGTH = 60;

    @Override
    public String getId() {
        return ID;
    }

    @Override
    protected void doValidate(String name, String inputHint, ValidationContext context, ValidatorConfig config) {
        String result;
        try {
            result = validateUsername(name);
        }
        catch (UnsupportedEncodingException e) {
            result = MESSAGE_INVALID_ENCODING;
        }
        if (result != OK) {
            context.addError(new ValidationError(ID, inputHint, result, name));
        }
    }

    /**
     * Actual username validation.
     *
     * Using a public method just for unit test convenience.
     *
     * java.util.regex.Matcher's find() is used instead of matches(), to behave
     * like PHP's preg_match() over regular expressions.
     */
    public String validateUsername(String name) throws UnsupportedEncodingException {
        if (name.length() == 0) {
            return MESSAGE_NOT_SET;
        }
        if (name.startsWith(" ")) {
            return MESSAGE_STARTS_WITH_SPACE;
        }
        if (name.endsWith(" ")) {
            return MESSAGE_ENDS_WITH_SPACE;
        }
        if (name.contains("  ")) {
            return MESSAGE_TWO_SPACES;
        }
        if (matchesRegex1(name)) {
            return MESSAGE_ILLEGAL_CHAR_1;
        }
        String regex2 = "["
            + "\\x80-\\xA0"     // Non-printable ISO-8859-1 + NBSP
            + "\\xAD"           // Soft-hyphen
            + "\\u2000-\\u200F" // Various space characters
            + "\\u2028-\\u202F" // Bidirectional teut overrides
            + "\\u205F-\\u206F" // Various teut hinting characters
            + "\\uFEFF"         // Byte order mark
            + "\\uFF01-\\uFF60" // Full-width latin
            + "\\uFFF9-\\uFFFD" // Replacement characters
            + "\\x00-\\x1F"     // NULL byte and control characters
            + "]";
        if (Pattern.compile(regex2).matcher(name).find()) {
            return MESSAGE_ILLEGAL_CHAR_2;
        }
        if (name.length() > USERNAME_MAX_LENGTH) {
            return MESSAGE_TOO_LONG;
        }
        return OK;
    }

    /**
     * Validates D7 first regex.
     *
     * Namely the php equivalent of
     * preg_match('/[^\x{80}-\x{F7} a-z0-9@+_.\'-]/i', $name)
     *
     * The reason this is separated is because Java regular expressions are not
     * able to process a byte stream like in PHP.
     * This regular expression may not be right, but it is the one used on
     * Drupal 7.
     * It matches based on decomposed UTF8 encoded bytes, since the "u"
     * modifier is not passed.
     */
    public boolean matchesRegex1(String name) throws UnsupportedEncodingException {
        // Part 1: equivalent to preg_match('/[^ a-z0-9@+_.\'-]/i', $name), including U+0080 to U+00F7 characters.
        String simple_allow_list_regex = "[^\\x80-\\xF7 a-z0-9@+_.'\\-]";
        int simple_allow_list_regex_flags = Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.UNICODE_CHARACTER_CLASS;
        boolean matches_simple_allow_list = Pattern
            .compile(simple_allow_list_regex, simple_allow_list_regex_flags)
            .matcher(name)
            .find();
        if (!matches_simple_allow_list) {
            // Name is in a simple character subset, passes, hence no match.
            return false;
        }
        // Part 2: decomposing of UTF8 bytes matching U+0080 to U+00F7 range.
        String simple_allow_list_regex_for_replace = "[ a-z0-9@+_.'\\-]";
        String non_simple_characters = Pattern
            .compile(simple_allow_list_regex_for_replace, simple_allow_list_regex_flags)
            .matcher(name)
            .replaceAll("");
        byte[] bytes = non_simple_characters.getBytes("UTF8");
        int unsigned_byte;
        for (int i=0; i < bytes.length; i++) {
            unsigned_byte = Byte.toUnsignedInt(bytes[i]);
            // out of 128-247 range
            if (unsigned_byte < 0x80 || unsigned_byte > 0xF7) {
                // Matches: outside the known range.
                return true;
            }
        }
        return false;
    }

    @Override
    public String getHelpText() {
        return "D7 username validator";
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return Collections.emptyList();
    }
}
